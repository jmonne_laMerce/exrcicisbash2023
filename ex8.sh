#!/bin/bash
# 8. Reescriu l'exercici 4 usant l'estructura CASE.

echo 1. crea directori
echo 2. crea fitxer
echo 3. borra directori
echo 4. borra fitxer
echo 
read -p "opció:" op

case $op in 

  1)
      read -p "nom:" nom
      mkdir $nom
    ;;
  2) 
      read -p "nom:" nom
      touch $nom
    ;;
  3)
      read -p "nom:" nom
      rmdir $nom
    ;;
  4)
      read -p "nom:" nom
      rm $nom
    ;;
  *)
    ;;
esac
