#!/bin/bash
# 16. Ajunta els scripts 12 a 15 en un sol script amb 4 opcions més una 5a que sigui "Sortir". Es permetrà executar successivament diferents opcions fins que s'esculli l'opció sortir. Si s'escull una opció incorrecta es demanarà una nova opció i es mostrarà un missatge per pantalla. Usa l'estructura case per al bucle principal i una funció per a cada opció.


# posem els exercicis a executar, seria més ideal posar el codi de cada exercici
ex12(){
    ./ex12.sh
}

ex13(){
    ./ex13.sh
}

ex14(){
    ./ex14.sh
}

ex15(){
    ./ex15.sh
}

errors=0
op=0

while [ $op != s ]
do
  echo "1) Crear usuaris successius"
  echo "2) Crear usuaris agafant noms d'un fitxer"
  echo "3) Eliminar usuaris successius"
  echo "4) Eliminar usuaris agafant noms d'un fitxer"
  echo "s) Sortir"
  read op

# comprovem que no està buit
  if [ -z $op ]
  then
    op=0
  fi

  case $op in
  1)
    ex12
  2)
    ex13
  3)
    ex14
  4)
    ex15
  s)
    exit
  *)
    printf "Opció incorrecta";;
  esac

done
