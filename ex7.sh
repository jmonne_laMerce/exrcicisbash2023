#!/bin/bash
# 7. Crea un script que faci un ls recursiu desde l'arrel del sistema de fitxers i ho guardi en un fitxer de text anomentat fotografia_20140130_1729.txt. 20140130_1729 ha de correspondre a la data i hora actuals del sistema en format [any][mes][dia]_[hora][minut].

ls -R / > fotografia_`date +%Y%m%d_%H%M`.txt
