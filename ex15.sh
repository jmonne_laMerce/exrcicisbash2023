#!/bin/bash
# 15. Eliminació massiva d'usuaris. Es demanarà el nom d'un fitxer de text que contindrà els noms dels usuaris a eliminar. Es preguntarà també si es vol eliminar o conservar els directoris personals. Abans de començar l'eliminació s'informarà a l'usuari dels noms dels usuaris a eliminar i es demanarà confirmació.

read -p "nom del fitxer amb els noms: " fitxer

if ! [ -f $fitxer ]
then 
    echo "El fitxer $fitxer no existeix"
    return 1
fi

echo "S'eliminaran els usuaris: "
for usu in `cat $fitxer`
do
    printf "$usu "
done

printf "\n"
read -p "Vols continuar? (s/n) " continuar

if [ $continuar = s ]
then
read -p "Eliminar directoris personals? (s/n) " del
for usu in `cat $fitxer`
do
  echo "Eliminant l'usuari $usu"
  if [ $del = s ]
  then
    userdel -r $usu > /dev/null 2>&1
  else
    userdel  $usu > /dev/null 2>&1
  fi
  if (( $? != 0 )) 
  then
    echo "Error eliminant l'usuari $usu" >&2
    return 1
  fi
done
fi
