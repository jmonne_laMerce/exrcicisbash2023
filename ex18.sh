#!/bin/bash
# 18. Modifica el script de l'exercici anterior de forma que la informació que vagi a la sortida standard es mostri en color verd i la infomació que vagi a l'error standard es mostri en color vermell.

#definim quins colors volem
VERD='\033[0;32m'
VERMELL='\033[0;31m'
SENSE_COLOR='\033[0m'


# Definim una funció per mostrar la informació en verd
function success_msg() {
  echo -e "${VERD}$1${SENSE_COLOR}"
}

# Definim una funció per mostrar l'error en vermell
function error_msg() {
  >&2 echo -e "${VERMELL}$1${SENSE_COLOR}"
}

ex12(){
    read -p "nom grup: " grup

if [ $(getent group $grup ]
then
    echo error_msg "el grup existeix" && errors+=1
else
    addgroup $grup
    echo "grup creat"
    read -p "nom usuari: " nom
    read -sp "Contrassenya: " pass
    read -p "quantitat d'usuaris: " num

    for ((i=1; i<=$num; i++))
    do
        if [ $(getent passwd $nom$i ]
            then
                echo error_msg "usuari $nom$i existex" && errors+=1
            else
                useradd -m -g $grup -p $pass $nom$i
                echo succes_msg "s'ha creat l'usuari $nom$1"
        fi
done
fi

}

ex13(){
if [ $(getent group $grup ]
then
    echo error_msg "el grup existeix" && errors+=1
else
    addgroup $grup
    echo "grup creat"
    read -p "Nom del fitxer amb els noms: " fitxer
    read -sp "Contrassenya: " pass

    for nom in `cat $fitxer`
    do
        if [ $(getent passwd $nom$i ]
            then
                echo error_msg "usuari $nom$i existex" && errors+=1
            else
                useradd -m -g $grup -p $pass $nom
                echo succes_msg "s'ha creat l'usuari $nom"
        fi
fi
}

ex14(){
read -p "Nom dels usuaris a eliminar:" usuari
read -p "Nombre d'usuaris a eliminar:" max

echo "S'eliminaran els usuaris: "

for ((i=1;i<=$max;i++))
do
  printf "$usuari$i "
done

printf "\n"
read -p "Vols continuar? (s/n)" continuar

if [ $continuar = s ]
then
  read -p "Eliminar directoris personals? (s/n)" del
  for ((i=1;i<=$max;i++))
  do
    printf "Eliminant l'usuari $usuari$i\r"
    if [ $del = s ]
    then
      userdel -r $usuari$i
    else
      userdel  $usuari$i
    fi
  done
fi
}

ex15(){
    read -p "nom del fitxer amb els noms: " fitxer

if ! [ -f $fitxer ]
then 
    echo error_msg "El fitxer $fitxer no existeix" && errors+=1
    return 1
fi

echo "S'eliminaran els usuaris: "
for usu in `cat $fitxer`
do
    printf "$usu "
done

printf "\n"
read -p "Vols continuar? (s/n) " continuar

if [ $continuar = s ]
then
read -p "Eliminar directoris personals? (s/n) " del
for usu in `cat $fitxer`
do
  echo "Eliminant l'usuari $usu"
  if [ $del = s ]
  then
    userdel -r $usu > /dev/null 2>&1
  else
    userdel  $usu > /dev/null 2>&1
  fi
  if (( $? != 0 )) 
  then
    echo error_msg "Error eliminant l'usuari $usu" >&2
    return 1
  fi
done
}

errors=0
op=0

while [ $op != s ]
do
  echo "1) Crear usuaris successius"
  echo "2) Crear usuaris agafant noms d'un fitxer"
  echo "3) Eliminar usuaris successius"
  echo "4) Eliminar usuaris agafant noms d'un fitxer"
  echo "s) Sortir"
  read op

# comprovem que no està buit
  if [ -z $op ]
  then
    op=0
  fi

  case $op in
  1)
    ex12
    errors=$(($errors+$?));;
  2)
    ex13
    errors=$(($errors+$?));;
  3)
    ex14
    errors=$(($errors+$?));;
  4)
    ex15
    errors=$(($errors+$?));;
  s)
    exit $errors;;
  *)
    printf "Opció incorrecta";;
  esac

done
