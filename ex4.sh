#!/bin/bash
# 4. Crea un script que mitjançant un menú amb quatre opcions permeti crear un directori, crear un fitxer, borrar un directori o borrar un fitxer.

echo 1. crea directori
echo 2. crea fitxer
echo 3. borra directori
echo 4. borra fitxer
echo 
read -p "opció:" op

if [ $op -eq  1 ]
then
  read -p "nom:" nom
  mkdir $nom
fi

if [ $op -eq  2 ]
then
  read -p "nom:" nom
  touch $nom
fi

if [ $op -eq  3 ]
then
  read -p "nom:" nom
  rmdir $nom
fi

if [ $op -eq  4 ]
then
  read -p "nom:" nom
  rm $nom
fi
