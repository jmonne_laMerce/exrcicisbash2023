#!/bin/bash
# 2. Script que demani un nom de fitxer o directori i ens digui si existeix o no i si és un fitxer o un directori.

echo Introdueix un nom de fitxer o directori:
read nom
 
if [ -d $nom ]
then
  echo $nom existeix i és un directori
else
  if [ -f $nom ]
  then
    echo $nom existeix i és un fitxer
  else
    echo $nom no existeix
  fi
fi
