#!/bin/bash
# 1. Script que demani un nom de fitxer o directori i ens digui si existeix o no.

echo Introdueix un nom de fitxer o directori:
read nom
 
if [ -e $nom ]
then
  echo $nom existeix
else
  echo $nom no existeix
fi
