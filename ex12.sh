#!/bin/bash
# 12. Script de creació massiva d'usuaris. Es demanarà per teclat un grup, un nom d'usuari, una contrassenya i el nombre d'usuaris a crear. Es crearà el nombre d'usuaris indicats amb la contrassenya entrada per teclat i amb el grup principal indicat (que es crearà si no existeix). Si per exemple s'entra usu com a nom d'usuari i s'han de crear 200 usuaris, els noms dels usuaris seran usu1, usu2, usu3, ... , usu200. La creació dels usuaris ha d'incloure la creació del directori personal.

read -p "nom grup: " grup

if [ $(getent group $grup ]
then
    echo "el grup existeix"
else
    addgroup $grup
    echo "grup creat"
    read -p "nom usuari: " nom
    read -sp "Contrassenya: " pass
    read -p "quantitat d'usuaris: " num

    for ((i=1; i<=$num; i++))
    do
        if [ $(getent passwd $nom$i ]
            then
                echo "usuari $nom$i existex"
            else
                useradd -m -g $grup -p $pass $nom$i
                echo "s'ha creat l'usuari $nom$1"
        fi
done
fi

