#!/bin/bash
# 5. Afegeix a l'exercici anterior un control d'opció correcta/incorrecta i controla si s'han realitzat correctament les operacions. En qualsevol cas d'error se sortirà del script amb un codi d'error diferent de zero.

echo 1. crea directori
echo 2. crea fitxer
echo 3. borra directori
echo 4. borra fitxer
echo 
read -p "opció:" op

if [ $op -eq  1 ]
then
  read -p "nom:" nom
  mkdir $nom
  if [ "$?" -eq 0 ]; then
    echo "El directori $nom s'ha creat correctament."
  else
    >&2 echo "Error: no s'ha pogut crear el directori $nom."
    exit 1
  fi
fi

if [ $op -eq  2 ]
then
  read -p "nom:" nom
  touch $nom
  if [ "$?" -eq 0 ]; then
    echo "El fitxer $nom s'ha creat correctament."
  else
    >&2 echo "Error: no s'ha pogut crear el fitxer $nom."
    exit 1
  fi
fi

if [ $op -eq  3 ]
then
  read -p "nom:" nom
  rmdir $nom
  if [ "$?" -eq 0 ]; then
    echo "El directori $nom s'ha esborrat correctament."
  else
    >&2 echo "Error: no s'ha pogut esborrar el directori $nom."
    exit 1
  fi
fi

if [ $op -eq  4 ]
then
  read -p "nom:" nom
  rm $nom
  if [ "$?" -eq 0 ]; then
    echo "El fitxer $nom s'ha esborrat correctament."
  else
    >&2 echo "Error: no s'ha pogut esborrar el fitxer $nom."
    exit 1
  fi
fi
