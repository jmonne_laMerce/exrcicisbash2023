#!/bin/bash
# 13. Script de creació massiva d'usuaris. Es demanarà el grup principal dels usuaris que es crearà si no existeix. Es demanarà el nom d'un fitxer de text que conté el nom dels usuaris a crear. I finalment es demanarà també la contrassenya per a tots els usuaris. El script crearà tots els usuaris amb el seu directori principal i la contrassenya indicada.

if [ $(getent group $grup ]
then
    echo "el grup existeix"
else
    addgroup $grup
    echo "grup creat"
    read -p "Nom del fitxer amb els noms: " fitxer
    read -sp "Contrassenya: " pass

    for nom in `cat $fitxer`
    do
        if [ $(getent passwd $nom$i ]
            then
                echo "usuari $nom$i existex"
            else
                useradd -m -g $grup -p $pass $nom
                echo "s'ha creat l'usuari $nom"
        fi
fi

