#!/bin/bash
# 14. Eliminació massiva d'usuaris. Es demanarà el nom dels usuaris i el nombre d'usuaris a eliminar. Per exemple si s'entra usu com a nom d'usuari i 200 com a nombre d'usuaris, els noms dels usuaris a eliminiar seran usu1, usu2, usu3, ... , usu200. Es preguntarà també si es vol eliminar o conservar els directoris personals. Abans de començar l'eliminació s'informarà a l'usuari dels noms dels usuaris a eliminar i es demanarà confirmació.

read -p "Nom dels usuaris a eliminar:" usuari
read -p "Nombre d'usuaris a eliminar:" max

echo "S'eliminaran els usuaris: "

for ((i=1;i<=$max;i++))
do
  printf "$usuari$i "
done

printf "\n"
read -p "Vols continuar? (s/n)" continuar

if [ $continuar = s ]
then
  read -p "Eliminar directoris personals? (s/n)" del
  for ((i=1;i<=$max;i++))
  do
    printf "Eliminant l'usuari $usuari$i\r"
    if [ $del = s ]
    then
      userdel -r $usuari$i
    else
      userdel  $usuari$i
    fi
  done
fi

