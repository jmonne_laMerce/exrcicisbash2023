#!/bin/bash
# 6. Exercici de substitució de comandes. Crea un script que digui la data i hora actuals amb el següent format: Avui és dijous 30 de gener de 2014 i són les 17:04. 

# Opció A
echo Avui és `date +%A`, `date +%d` de `date +%B` de `date +%Y` i són les `date +%R`

# Opció B
echo Avui és `date +"%A"`, `date +"%d"` de `date +"%B"` de `date +"%Y"` i són les `date +"%R"`
