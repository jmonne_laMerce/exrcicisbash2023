#!/bin/bash
# 3. Crea un script que digui si un fitxer o directori té permisos de lectura, escriptura i/o execució.

echo nom del fitxer/directori  
read nom                       

if [ -e $nom ]
then
  echo $nom té els permisos:
  if [ -r $nom ]
  then
    echo lectura
  fi
  if [ -w $nom ]
  then
    echo escriptura
  fi
  if [ -x $nom ]
  then
    echo execució
  fi
else
  echo $nom no existeix
fi
